ADT {
	var <folderPath, <directions, match, order, format, filename, ymlOnly;
	var adtDecoderPath;
	var directionsPath, command, paramArr;
	var hOrder, vOrder, decoderType, mixedOrderScheme;
	classvar <>userOctaveCmd, <>userADTDir, <>userPyADTDir, <>scripts;

	*initClass {
		userADTDir = File.realpath(ADT.filenameSymbol.asString.dirname ++ "/../../../Tools/adt/");
		userOctaveCmd = "which octave".unixCmdGetStdOut.replace($\n);
		userPyADTDir = File.realpath(ADT.filenameSymbol.asString.dirname ++ "/../../../Tools/adt_evaluation/");
		scripts = File.realpath(ADT.filenameSymbol.asString.dirname +/+ "../scripts");
	}

	*new { |folderPath, directions = ([[ 0, 0, 1 ]]), match = \amp, order = 3, format = (['ACN', 'N3D']), filename = 'ADT', ymlOnly = true|
		^super.newCopyArgs(folderPath, directions, match, order, format, filename, ymlOnly).init
	}

	init {

		this.prParseOrder(order);

		// write to atk extensions folder
		folderPath = folderPath ?? {
			Quarks.isInstalled("atk-sc3").if({
				Atk.userExtensionsDir ++ "/matrices/HOA" ++ hOrder ++ "/decoders/"
			}, {
				ADT.filenameSymbol.asString.dirname ++ "/../decoders/"
			})
		};

		// make paths
		folderPath.mkdir;
		adtDecoderPath = ADT.userADTDir +/+ "decoders/";
		adtDecoderPath.mkdir;

		directions = directions.rank.switch(
			0, { Array.with(directions, 0.0, 1.5).reshape(1, 3) },
			1, { directions.collect({ |dir| Array.with(dir, 0.0, 1.5)}) },
			2, { directions },
			{ Error("Invaid directions format. Supplied Array with rank " ++ directions.rank ++ "." + "Directions should be rank 2 or lower").throw }
		);
		directions = directions.collect({ |thetaPhiRho|  // wrap to [ +/-pi, +/-pi/2 ]
			var sphere;
			sphere = Spherical.new(thetaPhiRho.at(2) ?? {1.5}, thetaPhiRho.at(0), thetaPhiRho.at(1)).asCartesian.asSpherical;
			sphere.angles ++ sphere.rho
		});

		// collect dirs into txt file to load into ADT
		directionsPath = Platform.defaultTempDir ++ "directions.txt";
		this.prADTSaveDirs(directionsPath);
		directionsPath = directionsPath.escapeChar($ );

	}

	// save directions to text file for use with Matlab - ADT
	prADTSaveDirs { |tmpPath|
		var file;

		file = File.new(tmpPath, "w");
		directions.do{ |dirs|
			// write directions with trailing space
			dirs.do{ |dir|
				file.write(dir.asString ++ " ");
			};
			// write new line
			file.write("\n");
		};
		file.close;
	}

	prParseImagSpkrs { |imagSpeakers|
		var cartesians, string;

		// check for shape of supplied imaginary speaker directions
		cartesians = imagSpeakers.rank.switch(
			0, { Array.with(imagSpeakers, 0.0, 1.5).reshape(1, 3) }, // single value
			1, { imagSpeakers.collect({ |dir| Array.with(dir, 0.0, 1.5)}) }, // 1D array for 2D speakers
			2, { imagSpeakers }, // 3D speakers
		).collect({ |thetaPhiRho|  // wrap to [ +/-pi, +/-pi/2 ]
			Spherical.new(thetaPhiRho.at(2), thetaPhiRho.at(0), thetaPhiRho.at(1)).asCartesian
		});

		// format for use with matlab, e.g. 2D array = [1 0 0; 1 1 0]
		string = "[";
		cartesians.do{ |cart|
			cart.asArray.do{ |val|
				string = string + val.round(0.0001) // round for numbers close to zero
			};
			string = string ++ ";"
		};
		^string = (string ++ "]").asCompileString;
	}

	prParseElevationRange { |elevationRange|
		var string;

		string = "[";

		elevationRange.notNil.if({

			elevationRange.rank.switch(
				0, { string = string + elevationRange.raddeg },
				1, {
					elevationRange.do{ |range|
						string = string + range.raddeg;
					}
				}
			);

		});

		^string = string + "]"

	}

	prParseOrder {
		// split order if its an array, if not give same order for horizontal and vertical
		order.isKindOf(Array).if({
			hOrder = order[0];
			vOrder = order[1];
			mixedOrderScheme = order[2];
		}, {
			hOrder = order;
			vOrder = order;
			mixedOrderScheme = 'amb';
		});

		decoderType = (hOrder == vOrder).if({ 1 }, { 2 });

	}

	allradPython { |imagSpeakers = true, verbose = false|
		var cmd;
		cmd = Python.pathToPython + ADT.scripts.escapeChar($ ) +/+ "ADT_Python.py" + hOrder + vOrder + imagSpeakers + directionsPath + folderPath.escapeChar($ ) + filename + ADT.userPyADTDir.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd;
	}

	allrad { |imagSpeakers, type = 'python', verbose = false|

		(type == 'python').if({
			this.allradPython(imagSpeakers, verbose)
		}, {

			// parse imaginary speakers for matlab format, empty array if nil
			imagSpeakers = imagSpeakers.notNil.if({ this.prParseImagSpkrs(imagSpeakers) }, { "[]".asCompileString });

			this.run("allrad", imagSpeakers, verbose)
		})
	}

	pinv { |alpha, verbose = false|

		this.run("pinv", alpha, verbose)

	}

	ssf { |alpha, elevationRange, verbose = false|

		elevationRange = this.prParseElevationRange(elevationRange).asCompileString;

		this.run("ssf", [alpha, elevationRange], verbose)

	}

	run { |type = "allrad", extraArgs, verbose = false|
		// init command to octave plus matlab (octave) script
		command = ADT.userOctaveCmd + ADT.userADTDir.escapeChar($ ) +/+ "examples/run_atk_" ++ type ++ ".m";

		// collect params
		paramArr = [hOrder, vOrder, mixedOrderScheme, format[0], format[1], decoderType, directionsPath, match].insert(2, extraArgs);
		(type == "ssf").if({ paramArr[7] = [paramArr[7]]; paramArr = paramArr.flatten; paramArr.remove(decoderType) });

		// append params to command
		paramArr.do{ |param|
			command = command + param;
		};

		// append decoder write path
		command = command + (folderPath ++ filename ++ "-" ++ type).escapeChar($ );
		ymlOnly.if({
			command = command + adtDecoderPath ++ filename ++ "-" ++ type;
		}, {
			command = command + (folderPath ++ filename ++ "-" ++ type).escapeChar($ );
		});
		verbose.if({ command.postln });
		command.systemCmd

	}

	filePath { |type = "allrad", beam = "energy"|
		^case
		{ beam == "energy" }
		{ folderPath ++ filename ++ "-" ++ type ++ "-beam-" ++ beam ++ "-match-" ++ match ++ ".yml" }
		{ folderPath ++ filename ++ "-" ++ type ++ "-beam-" ++ beam ++ "-match-amp.yml" }

	}



	matrixPaths {
		^PathName(this.folderPath).files.collect{ |pathname|
			pathname.fullPath
		}
	}

	matrixNames {
		^PathName(this.folderPath).files.collect{ |pathname|
			pathname.fileName
		}
	}
}