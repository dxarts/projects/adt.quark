import os
import csv
import argparse
import sys


parser = argparse.ArgumentParser(description='Run ADT from SuperCollider')

parser.add_argument('h_order', type=int,
                help='horizontal ambisonic order')
parser.add_argument('v_order', type=int,
                help='vertical ambisonic order')
parser.add_argument('imag_spkrs', type=str,
                help='boolean to add imaginary speakers')
parser.add_argument('directions_path', type=str,
                help='path to directions text file')
parser.add_argument('out_path', type=str,
                help='output folder path')
parser.add_argument('name', type=str,
                help='name of the output')
parser.add_argument('adt_path', type=str,
                help='path to ADT repo')
args = parser.parse_args()
h_order = args.h_order
v_order = args.v_order
imag_spkrs = args.imag_spkrs == "true"
directions_path = args.directions_path
out_path = args.out_path
name = args.name
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, args.adt_path)
import basic_decoders as bd
import example_speaker_arrays as esa
import program_channels as pc
import atk_interface as atk
import example_speaker_arrays as esa
import loudspeaker_layout as lsl

spkr_arr = []
with open(directions_path) as f:
    csvreader = csv.reader(f, delimiter="\t")
    for row in csvreader:
        for val in [float(string) for string in row[0].split()]:
            spkr_arr.append(val)

S = lsl.from_array(spkr_arr, coord_code='AER', unit_code='RRM', name=name)
if imag_spkrs:
    S += esa.nadir(radius=1.5, is_imaginary=True)
    dirs = [S.az[:-1], S.el[:-1]]
else:
    dirs = [S.az, S.el]

C = pc.ChannelsN3D(h_order, v_order)
order_h, order_v, sh_l, sh_m, id_string = pc.ambisonic_channels(C)


M_allrad = bd.allrad(sh_l, sh_m, S.az, S.el, speaker_is_real=S.is_real)

out_path = os.path.join(out_path, S.name+"-order-"+str(h_order)+"-allrad-beam-energy-match-amp.yml")

atk_signal_set = 'HOA' + str(h_order)

atk.write_atk_yml(out_path, M_allrad, dirs[0], dirs[1], atk_signal_set)
