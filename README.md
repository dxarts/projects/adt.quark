# ADT

_Run the Ambisonic Decoder Toolbox from within SuperCollider._

## How to use:

After installing this Quark, you should clone the [ADT repo](https://bitbucket.org/ambidecodertoolbox/adt/src/master/).

You will also need to install [Octave](https://www.gnu.org/software/octave/).

Or if you use [Homebrew](https://brew.sh/):
```
brew install octave
```

After installing octave, in SuperCollider check that octave is in the path:
```supercollider
"which octave".unixCmd
```
This should return something like "/usr/local/bin/octave". If this returns nothing, the path to the octave command is not in your PATH variable. Go to Terminal:

```
$ which octave
```

You can set the class variable of ADT in SuperCollider to this path:
```supercollider
ADT.userOctaveCmd = "/usr/local/bin/octave"; // insert your own path
```

Otherwise if you want to add the entire path in SuperCollider you can add this to your PATH variable (replacing "/usr/local/bin:" with the output from the above Terminal command):
```supercollider
"PATH".setenv("/usr/local/bin:" ++ "PATH".getenv); // set the PATH variable to include the path to the folder with Octave
```

-----
## Requirements:
- [ADT](https://bitbucket.org/ambidecodertoolbox/adt/src/master/)
- [Octave](https://www.gnu.org/software/octave/)
